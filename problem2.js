// Check the each person projects list and group the data based on the project status and return the data as below
  
  /*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
*/
  
function getProjectStatus(dataSet){
    if(Array.isArray(dataSet)){
        let projectStatus = dataSet.reduce((acc, person) => {
            person.projects.forEach(project => {
              if (!acc[project.status]){
                acc[project.status] = [];
              }
              acc[project.status].push(project.name);
            });
            return acc;
        }, {});
        return projectStatus;
    }
    else{
        return [];
    }
}

module.exports = getProjectStatus;