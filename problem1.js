// Iterate over each object in the array and in the email breakdown the email and return the output as below:
  /*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
*/

function getEmailData(dataSet){
    if(Array.isArray(dataSet)){
        
        let personData = dataSet.map((data) => {
            let name = data.name.split(' ');
            let mail = data.email.split('@');
            let personEmailBreakdown = {};
            personEmailBreakdown.firstName = name[0];
            personEmailBreakdown.lastName = name[1];
            personEmailBreakdown.emailDomain = mail[1];
            return personEmailBreakdown;
        });
        return personData;
    }
    else{
        return [];
    }
}

module.exports = getEmailData;