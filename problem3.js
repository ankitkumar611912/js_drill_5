// From the given data, filter all the unique langauges and return in an array

function getUniqueLanguages(dataSet){
    if(Array.isArray(dataSet)){
        let uniqueLanguages = dataSet.map((data) => {
            return data.languages;
        });
        uniqueLanguages = uniqueLanguages.flat(1);
        uniqueLanguages = [...new Set(uniqueLanguages)];
        return uniqueLanguages;
    }else{
        return [];
    }
}
module.exports = getUniqueLanguages;